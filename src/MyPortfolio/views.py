__author__ = "BARABITE-DUBOUCH Bastien"
__copyright__ = "Copyright (C) 2021 BARABITE-DUBOUCH Bastien"
__license__ = "Private"
__version__ = "0.1"

from datetime import datetime
from django.shortcuts import render


def index(request):
    first_name = "bastien"
    date = datetime.today()

    nav = ["Accueil", "Mon Portefolio", "Mon Travail", "Contact"]

    main = "Concepteur de Systèmes d'Informations"

    copy_right = "BARABITE-DUBOUCH Bastien"

    return render(request, "MyPortfolio/index.html", context={"date": date,
                                                              "first_name": first_name,
                                                              "copyright": copy_right})
